# nFrames-PixElement API Spec


## Shared AWS resources

S3 bucket to store completed data: `nframes-pixelement`

SQS queue for message passing to trigger events: `nframes-pixelement-messages`

## General Workflow

1. Client image upload
2. Pix bundle adjustment process
3. Upload orientation file to shared S3 bucket
4. Send POST to nframes endpoint containing all relevant project initialization values
5 Watch SQS queue for product progress and product completion updates
6. Live update progress bars and products as they finish and are ready for visualization
7. Send customers emails as products finish (if they wanted progress updates)


## Message passing details

In general the workflow should go like this for all messages:
- Upload any relevant files to their predetermined location in the shared S3 bucket
- Send JSON message to shared SQS queue
```mermaid
graph LR
PIX[PixElement]
SQS((SQS queue))
S3((S3 bucket))
N[nFrames] --> PC{Product Completed}
N[nFrames] --> PU{Progress update}
PC -- Upload files -->S3
PU -- Send message-->SQS
S3 -- Send message-->SQS
SQS-- Consume message -->PIX
```

### Pixelement to nframes initialization POST:

##### Endpoint URL
`TBD`

##### S3 bucket file location
	nframes-pixelement/<image_set_id>/input/bundle_orientations_<bundle_id>.txt

##### POST request body
	{
		'imageset_id':      <string>  used to identify project in shared S3 bucket'
		'wkt':              <string>  wkt string used for project coordinate system definition'
		'unit_of_measure':  <integer> identifying the units to be used in output products.
                                      0 - meters
                                      1 - international feet
                                      2 - U.S. Survey feet
		'bundle_id':        <string>  unique identifier used on all files related to the bundle adjustment
		'dense_id':         <string>  unique identifier used on all files related to the dense point cloud
		'surface_id':       <string>  unique identifier used on all files related to the texture mesh
		'dem_id':           <string>  unique identifier used on all files related to the digital elevation model
		'ortho_id':         <string>  unique identifier used on all files related to the orthoimage
	}


### nframes message passing to 'nframes-pixelement-messages' SQS queue

##### Progress updates JSON:

	{
		'task':                <string> 'record_progress'
		'product_type':        <string> identifier for which type of product's progress is to be updated.  One of:
		                                'dense_point_cloud'
		                                'surface'
		                                'digital_elevation_model'
		                                'orthoimage'
		'product_id':          <string> unique identifier for product to be updated
		'amount_to_increment': <float>  between 0-100 for amount of progress that has increased since the last progress update
	}

##### Product completion JSON:

	{
		'task':          <string> 'product_completed'
		'product_type':  <string> identifier for which type of product is to be updated.  One of:
		                          'dense_point_cloud'
		                          'surface'
		                          'digital_elevation_model'
		                          'orthoimage'
		'product_id':    <string> unique identifier for product that has completed
	}


## Completed product types and S3 locations

##### Dense Point Cloud

	las: nframes-pixelement/<image_set_id>/completed/dense_point_cloud_<product_id>.las.zip

##### Texture Mesh

	cesium: nframes-pixelement/<image_set_id>/completed/tileset_<product_id>.json

##### Digital Elevation Model


	tif:   nframes-pixelement/<image_set_id>/completed/dem_<product_id>.tif.zip
	tfw:   nframes-pixelement/<image_set_id>/completed/dem_<product_id>.tfw
	jpg:   nframes-pixelement/<image_set_id>/completed/dem_<product_id>.jpg
	jpgw:  nframes-pixelement/<image_set_id>/completed/dem_<product_id>.jpgw

##### Orthoimage

	tif:   nframes-pixelement/<image_set_id>/completed/orthoimage_<product_id>.tif.zip
	tfw:   nframes-pixelement/<image_set_id>/completed/orthoimage_<product_id>.tfw
	jpg:   nframes-pixelement/<image_set_id>/completed/orthoimage_<product_id>.jpg
	jpgw:  nframes-pixelement/<image_set_id>/completed/orthoimage_<product_id>.jpgw
